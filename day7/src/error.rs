use crate::process::Process;

#[derive(Debug)]
pub enum Error {
    ParseError,
    NeedInput(Process),
    OutOffBounds(String),
    ProgramFailed,
    NotFound,
    InvalidOpcode(String),
}
