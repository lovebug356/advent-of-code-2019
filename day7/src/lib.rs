#[cfg(test)]
mod tests;

pub mod error;
pub use error::Error;

pub mod amplifier;
pub mod instruction;
pub mod memory;
pub mod opcode;
pub mod phasegenerator;

pub mod process;
pub use phasegenerator::PhaseGenerator;
