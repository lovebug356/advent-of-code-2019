pub struct PhaseGenerator {
    size: usize,
    min: i32,
    max: i32,
}

impl PhaseGenerator {
    pub fn new(size: usize, min: i32, max: i32) -> Self {
        Self { size, min, max }
    }

    pub fn iter(&self) -> impl Iterator<Item = Vec<i32>> {
        let min = self.min;
        let max = self.max;
        let mut result: Box<dyn Iterator<Item = Vec<i32>>> =
            Box::new(single_phase_generator(min, max, vec![]));

        for _idx in 1..self.size {
            result =
                Box::new(result.flat_map(move |input| single_phase_generator(min, max, input)));
        }

        result
    }
}

fn single_phase_generator(min: i32, max: i32, input: Vec<i32>) -> impl Iterator<Item = Vec<i32>> {
    (min..max + 1).filter_map(move |idx| {
        if !input.iter().any(|&peer| peer == idx) {
            let mut result = input.clone();
            result.push(idx);
            Some(result)
        } else {
            None
        }
    })
}
