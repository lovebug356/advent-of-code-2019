use crate::memory::Memory;
use crate::Error;
use std::convert::TryFrom;

#[test]
fn create_memory() -> Result<(), Error> {
    let memory = Memory::try_from("0,1,2,3")?;
    assert_eq!(memory.read(0)?, 0);
    assert_eq!(memory.read(1)?, 1);
    assert_eq!(memory.read(2)?, 2);
    assert_eq!(memory.read(3)?, 3);

    Ok(())
}

#[test]
fn write_memory() -> Result<(), Error> {
    let memory = Memory::try_from("0,1,2")?;
    let memory = memory.write(1, 3)?;
    assert_eq!(memory.read(1)?, 3);

    let memory = memory.write(0, 4)?;
    assert_eq!(memory.read(0)?, 4);

    let memory = memory.write(2, 5)?;
    assert_eq!(memory.read(2)?, 5);

    Ok(())
}
