use crate::PhaseGenerator;

#[test]
fn create_generator() {
    let size = 1;
    let min = 0;
    let max = 2;

    let gen = PhaseGenerator::new(size, min, max);
    assert_eq!(
        gen.iter().collect::<Vec<_>>(),
        vec![vec![0], vec![1], vec![2]],
    );

    let size = 2;
    let min = 3;
    let max = 4;

    let gen = PhaseGenerator::new(size, min, max);
    assert_eq!(gen.iter().collect::<Vec<_>>(), vec![vec![3, 4], vec![4, 3]]);

    let size = 3;
    let min = 5;
    let max = 7;

    let gen = PhaseGenerator::new(size, min, max);
    assert_eq!(
        gen.iter().collect::<Vec<_>>(),
        vec![
            vec![5, 6, 7],
            vec![5, 7, 6],
            vec![6, 5, 7],
            vec![6, 7, 5],
            vec![7, 5, 6],
            vec![7, 6, 5]
        ]
    );
}
