use std::clone::Clone;

use crate::memory::Memory;
use crate::process::Process;
use crate::Error;
use crate::PhaseGenerator;

pub struct Amplifier {
    memory: Memory,
}

impl Amplifier {
    pub fn new(memory: Memory) -> Self {
        Self { memory }
    }

    pub fn run(&self, input: i32, phase: i32) -> Result<i32, Error> {
        let process = Process::new(self.memory.clone(), 0, vec![phase, input]);
        let process = process.run()?;
        Ok(process.output[0])
    }

    pub fn run_series(&self, input: i32, phases: &Vec<i32>) -> Result<i32, Error> {
        phases.iter().fold(Ok(input), |input, &phase| {
            input.and_then(|input| self.run(input, phase))
        })
    }

    pub fn run_series_loop(&self, input: i32, phases: &Vec<i32>) -> Result<i32, Error> {
        let mut procs = phases
            .iter()
            .map(|&phase| Process::new(self.memory.clone(), 0, vec![phase]))
            .collect::<Vec<_>>();

        let proc = procs.remove(0).append_input(vec![input]);
        procs.insert(0, proc);

        loop {
            let result = procs.remove(0);
            let result = result.run();
            match result {
                Ok(proc) => {
                    let (_proc, output) = proc.reset_output();
                    if procs.len() == 0 {
                        return Ok(output[0]);
                    }
                    let next = procs.remove(0);
                    procs.insert(0, next.append_input(output));
                }
                Err(err) => match err {
                    Error::NeedInput(proc) => {
                        let next = procs.remove(0);
                        let (proc, output) = proc.reset_output();
                        procs.insert(0, next.append_input(output));
                        procs.push(proc);
                    }
                    _ => return Err(err),
                },
            }
        }
    }

    pub fn search_max_output(&self) -> Result<i32, Error> {
        Ok(PhaseGenerator::new(5, 0, 4)
            .iter()
            .map(|phases| (phases.clone(), self.run_series(0, &phases)))
            .fold((vec![], 0), |best, (phases, result)| match result {
                Ok(result) => {
                    if result > best.1 {
                        (phases, result)
                    } else {
                        best
                    }
                }
                Err(_err) => best,
            })
            .1)
    }

    pub fn search_max_output_loop(&self) -> Result<i32, Error> {
        Ok(PhaseGenerator::new(5, 5, 9)
            .iter()
            .map(|phases| (phases.clone(), self.run_series_loop(0, &phases)))
            .fold((vec![], 0), |best, (phases, result)| match result {
                Ok(result) => {
                    if result > best.1 {
                        (phases, result)
                    } else {
                        best
                    }
                }
                Err(_err) => best,
            })
            .1)
    }
}
