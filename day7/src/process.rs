use std::convert::TryFrom;

use crate::instruction::Instruction;
use crate::instruction::ParameterMode;
use crate::memory::Memory;
use crate::opcode::Opcode;
use crate::Error;

#[derive(Debug, Clone)]
pub struct Process {
    pub memory: Memory,
    pub pointer: usize,
    pub input: Vec<i32>,
    pub output: Vec<i32>,
}

impl Process {
    pub fn new(memory: Memory, pointer: usize, input: Vec<i32>) -> Self {
        Self {
            memory,
            pointer,
            input,
            output: vec![],
        }
    }

    pub fn with_input(mut self, input: Vec<i32>) -> Self {
        self.input = input;
        self
    }

    pub fn append_input(mut self, input: Vec<i32>) -> Self {
        input.iter().for_each(|&e| self.input.push(e));
        self
    }

    pub fn reset_output(mut self) -> (Self, Vec<i32>) {
        let output = self.output;
        self.output = vec![];
        (self, output)
    }

    pub fn with_single_input(mut self, input: i32) -> Self {
        self.input = vec![input];
        self
    }

    pub fn pop_front_input(mut self) -> Result<(Self, i32), Error> {
        if self.input.len() == 0 {
            return Err(Error::NeedInput(self));
        }
        let next_input = self.input.remove(0);
        Ok((self, next_input))
    }

    pub fn with_memory(mut self, memory: Memory) -> Self {
        self.memory = memory;
        self
    }

    pub fn with_increased_pointer(mut self, offset: usize) -> Self {
        self.pointer += offset;
        self
    }

    pub fn write_memory(mut self, position: usize, value: i32) -> Result<Self, Error> {
        self.memory = self.memory.write(position, value)?;
        Ok(self)
    }

    pub fn run(self) -> Result<Process, Error> {
        let mut program = self;
        loop {
            let opcode = program.memory.read(program.pointer)?;
            let instruction = Instruction::try_from(opcode)?;
            program = match instruction.opcode {
                Opcode::Add => program.add()?,
                Opcode::Multiply => program.mul()?,
                Opcode::Save => program.save()?,
                Opcode::Output => program.output()?,
                Opcode::JumpIfTrue => program.jump_true()?,
                Opcode::JumpIfFalse => program.jump_false()?,
                Opcode::LessThan => program.less_than()?,
                Opcode::Equals => program.equals()?,
                Opcode::EndOfProgram => return Ok(program),
            }
        }
    }

    fn add(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let dest = self.memory.read(self.pointer + 3)? as usize;
        let result = param_1 + param_2;
        Ok(self
            .write_memory(dest, result)?
            .with_increased_pointer(Opcode::Add.size()))
    }

    fn mul(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let dest = self.memory.read(self.pointer + 3)? as usize;
        let result = param_1 * param_2;
        Ok(self
            .write_memory(dest, result)?
            .with_increased_pointer(Opcode::Multiply.size()))
    }

    fn save(self) -> Result<Process, Error> {
        let param_1 = self.memory.read(self.pointer + 1)?;
        let (result, next_input) = self.pop_front_input()?;
        Ok(result
            .write_memory(param_1 as usize, next_input)?
            .with_increased_pointer(Opcode::Save.size()))
    }

    fn output(mut self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        println!("Output: {}", param_1);
        self.output.push(param_1);
        Ok(self.with_increased_pointer(Opcode::Output.size()))
    }

    fn jump_true(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        Ok(Process {
            pointer: if param_1 == 0 {
                self.pointer + Opcode::JumpIfTrue.size()
            } else {
                self.read_param(1)? as usize
            },
            memory: self.memory,
            input: self.input,
            output: self.output,
        })
    }

    fn jump_false(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        Ok(Process {
            pointer: if param_1 == 0 {
                self.read_param(1)? as usize
            } else {
                self.pointer + Opcode::JumpIfFalse.size()
            },
            memory: self.memory,
            input: self.input,
            output: self.output,
        })
    }

    fn less_than(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let param_3 = self.memory.read(self.pointer + 3)? as usize;
        Ok(Process {
            pointer: self.pointer + Opcode::LessThan.size(),
            memory: self
                .memory
                .write(param_3, if param_1 < param_2 { 1 } else { 0 })?,
            input: self.input,
            output: self.output,
        })
    }

    fn equals(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let param_3 = self.memory.read(self.pointer + 3)? as usize;
        Ok(Process {
            pointer: self.pointer + Opcode::Equals.size(),
            memory: self
                .memory
                .write(param_3, if param_1 == param_2 { 1 } else { 0 })?,
            input: self.input,
            output: self.output,
        })
    }

    fn read_param(&self, idx: usize) -> Result<i32, Error> {
        let opcode = self.memory.read(self.pointer)?;
        let instruction = Instruction::try_from(opcode)?;
        let mode = &instruction.parameter_mode[idx];
        let position = self.memory.read(self.pointer + idx + 1)?;
        Ok(match mode {
            ParameterMode::PositionMode => self.memory.read(position as usize)?,
            ParameterMode::ImmediateMode => position,
        })
    }
}

impl From<Memory> for Process {
    fn from(memory: Memory) -> Self {
        Self::new(memory, 0, vec![0])
    }
}
