use crate::Error;
use std::convert::TryFrom;

#[derive(Debug, Clone, PartialEq)]
pub enum Memory {
    OrigData(Vec<i32>),
    WriteData {
        data: Box<Memory>,
        address: usize,
        new_value: i32,
    },
}

impl Memory {
    pub fn len(&self) -> usize {
        match &*self {
            Memory::OrigData(data) => data.len(),
            Memory::WriteData { data, .. } => data.len(),
        }
    }

    pub fn read(&self, address: usize) -> Result<i32, Error> {
        match &*self {
            Memory::OrigData(data) => {
                if address > data.len() {
                    return Err(Error::OutOffBounds(format!(
                        "Reading Out of bounds {} of {}",
                        address,
                        data.len()
                    )));
                }
                Ok(data[address as usize])
            }
            Memory::WriteData {
                data,
                address: write_address,
                new_value,
            } => {
                if address == *write_address {
                    Ok(*new_value)
                } else {
                    data.read(address)
                }
            }
        }
    }

    pub fn write(self, address: usize, new_value: i32) -> Result<Memory, Error> {
        if address > self.len() {
            return Err(Error::OutOffBounds(format!(
                "Writing Out of bounds {} of {}",
                address,
                self.len()
            )));
        }
        Ok(Memory::WriteData {
            data: Box::new(self),
            address,
            new_value,
        })
    }
}

impl TryFrom<&str> for Memory {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Ok(Memory::OrigData(
            value
                .split(",")
                .map(|s| s.parse::<i32>().map_err(|_e| Error::ParseError))
                .collect::<Result<_, _>>()?,
        ))
    }
}
