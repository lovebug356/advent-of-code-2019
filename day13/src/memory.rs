use crate::Error;
use std::convert::TryFrom;

#[derive(Debug, Clone, PartialEq)]
pub enum Memory {
    OrigData(Vec<i64>),
    WriteData {
        data: Box<Memory>,
        address: usize,
        new_value: i64,
    },
}

impl Memory {
    pub fn len(&self) -> usize {
        match &*self {
            Memory::OrigData(data) => data.len(),
            Memory::WriteData { data, .. } => data.len(),
        }
    }

    pub fn read(&self, address: usize) -> Result<i64, Error> {
        match &*self {
            Memory::OrigData(data) => {
                if address >= data.len() {
                    Ok(0)
                } else {
                    Ok(data[address as usize])
                }
            }
            Memory::WriteData {
                data,
                address: write_address,
                new_value,
            } => {
                if address == *write_address {
                    Ok(*new_value)
                } else {
                    data.read(address)
                }
            }
        }
    }

    pub fn write(self, address: usize, new_value: i64) -> Result<Memory, Error> {
        match self {
            Memory::OrigData(data) => Self::write_with_mod(data, address, new_value),
            Memory::WriteData { .. } => self.write_no_mod(address, new_value),
        }
    }

    fn write_with_mod(mut data: Vec<i64>, address: usize, new_value: i64) -> Result<Memory, Error> {
        if address < 10_000_000 {
            if address >= data.len() {
                data.resize(address + 1, 0);
            }
            data[address] = new_value;
            Ok(Memory::OrigData(data))
        } else {
            Memory::OrigData(data).write_no_mod(address, new_value)
        }
    }

    fn write_no_mod(self, address: usize, new_value: i64) -> Result<Memory, Error> {
        Ok(Memory::WriteData {
            data: Box::new(self),
            address,
            new_value,
        })
    }
}

impl TryFrom<&str> for Memory {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Ok(Memory::OrigData(
            value
                .trim()
                .split(",")
                .map(|s| s.parse::<i64>().map_err(|_e| Error::ParseError))
                .collect::<Result<_, _>>()?,
        ))
    }
}
