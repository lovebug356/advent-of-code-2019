use crate::Error;
use std::convert::TryFrom;

#[derive(Debug, Clone, PartialEq)]
pub enum Opcode {
    Add,
    Multiply,
    Save,
    Output,
    JumpIfTrue,
    JumpIfFalse,
    LessThan,
    Equals,
    AdjustRelativeBase,
    EndOfProgram,
}

impl Opcode {
    pub fn size(&self) -> usize {
        match self {
            Opcode::Add => 4,
            Opcode::Multiply => 4,
            Opcode::Save => 2,
            Opcode::Output => 2,
            Opcode::JumpIfTrue => 3,
            Opcode::JumpIfFalse => 3,
            Opcode::LessThan => 4,
            Opcode::Equals => 4,
            Opcode::AdjustRelativeBase => 2,
            Opcode::EndOfProgram => 1,
        }
    }
}

impl TryFrom<i64> for Opcode {
    type Error = Error;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        match value {
            1 => Ok(Opcode::Add),
            2 => Ok(Opcode::Multiply),
            3 => Ok(Opcode::Save),
            4 => Ok(Opcode::Output),
            5 => Ok(Opcode::JumpIfTrue),
            6 => Ok(Opcode::JumpIfFalse),
            7 => Ok(Opcode::LessThan),
            8 => Ok(Opcode::Equals),
            9 => Ok(Opcode::AdjustRelativeBase),
            99 => Ok(Opcode::EndOfProgram),
            _ => Err(Error::InvalidOpcode(format!("unknown opcode {}", value))),
        }
    }
}
