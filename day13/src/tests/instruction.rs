use std::convert::TryFrom;

use crate::instruction::Instruction;
use crate::instruction::ParameterMode;
use crate::opcode::Opcode;
use crate::Error;

#[test]
fn from_i32() -> Result<(), Error> {
    assert!(Instruction::try_from(-1).is_err());
    assert!(Instruction::try_from(0).is_err());

    assert!(Instruction::try_from(1)?.opcode == Opcode::Add);
    assert!(Instruction::try_from(1)?.parameter_mode == vec![ParameterMode::PositionMode; 3]);
    assert!(Instruction::try_from(2)?.opcode == Opcode::Multiply);
    assert!(Instruction::try_from(4)?.opcode == Opcode::Output);

    assert!(Instruction::try_from(101)?.opcode == Opcode::Add);
    assert!(Instruction::try_from(101)?.parameter_mode[0] == ParameterMode::ImmediateMode);
    assert!(Instruction::try_from(101)?.parameter_mode[1] == ParameterMode::PositionMode);
    assert!(Instruction::try_from(101)?.parameter_mode[2] == ParameterMode::PositionMode);

    let instruction = Instruction::try_from(1002)?;
    assert!(instruction.opcode == Opcode::Multiply);
    assert!(instruction.parameter_mode[0] == ParameterMode::PositionMode);
    assert!(instruction.parameter_mode[1] == ParameterMode::ImmediateMode);
    assert!(instruction.parameter_mode[2] == ParameterMode::PositionMode);

    Ok(())
}
