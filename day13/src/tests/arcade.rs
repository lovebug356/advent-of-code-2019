use std::convert::TryFrom;

use crate::Arcade;
use crate::Error;
use crate::TileId;

#[test]
fn tile_id_from_int() -> Result<(), Error> {
    assert_eq!(TileId::try_from(0)?, TileId::Empty);

    Ok(())
}

#[test]
fn draw_tiles() -> Result<(), Error> {
    let arcade = Arcade::new();

    let arcade = arcade.draw_output([1, 2, 3])?;
    let arcade = arcade.draw_output([6, 5, 4])?;

    assert_eq!(arcade.tiles.len(), 2);

    Ok(())
}

#[test]
fn empty_tile_instruction_not_in_tiles() -> Result<(), Error> {
    let arcade = Arcade::new();

    let arcade = arcade.draw_output([1, 2, 1])?;
    let arcade = arcade.draw_output([2, 2, 0])?;

    assert_eq!(arcade.tiles.len(), 1);

    let arcade = arcade.draw_output([1, 2, 0])?;

    assert_eq!(arcade.tiles.len(), 0);

    Ok(())
}

#[test]
fn count_block_tiles() -> Result<(), Error> {
    let arcade = Arcade::new();

    let arcade = arcade.draw_output([1, 2, 1])?;
    let arcade = arcade.draw_output([2, 2, 0])?;

    assert_eq!(arcade.count_block_tiles(), 0);

    let arcade = arcade.draw_output([2, 2, 2])?;
    let arcade = arcade.draw_output([2, 1, 2])?;

    assert_eq!(arcade.count_block_tiles(), 2);

    let arcade = arcade.draw_output([2, 1, 3])?;

    assert_eq!(arcade.count_block_tiles(), 1);

    Ok(())
}
