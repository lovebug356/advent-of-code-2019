use std::collections::HashMap;
use std::convert::TryFrom;
use std::convert::TryInto;

use crate::Error;
use crate::Memory;
use crate::Process;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum TileId {
    Empty,
    Wall,
    Block,
    HorizontalPaddle,
    Ball,
}

impl TryFrom<usize> for TileId {
    type Error = Error;

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(TileId::Empty),
            1 => Ok(TileId::Wall),
            2 => Ok(TileId::Block),
            3 => Ok(TileId::HorizontalPaddle),
            4 => Ok(TileId::Ball),
            _ => Err(Error::InvalidTileId(format!("unknown tile id {}", value))),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Position {
    pos_x: i64,
    pos_y: i64,
}

impl Position {
    pub fn new<I>(pos_x: I, pos_y: I) -> Self
    where
        I: Into<i64>,
    {
        Self {
            pos_x: pos_x.into(),
            pos_y: pos_y.into(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct DrawCommand {
    position: Position,
    tile_id: TileId,
}

impl TryFrom<[usize; 3]> for DrawCommand {
    type Error = Error;

    fn try_from(value: [usize; 3]) -> Result<Self, Self::Error> {
        Ok(DrawCommand {
            position: Position {
                pos_x: value[0] as i64,
                pos_y: value[1] as i64,
            },
            tile_id: TileId::try_from(value[2])?,
        })
    }
}

impl TryFrom<&[i64]> for DrawCommand {
    type Error = Error;

    fn try_from(value: &[i64]) -> Result<Self, Self::Error> {
        if value.len() != 3 {
            return Err(Error::ParseError);
        }
        Ok(DrawCommand {
            position: Position {
                pos_x: value[0],
                pos_y: value[1],
            },
            tile_id: TileId::try_from(value[2] as usize)?,
        })
    }
}

pub struct Arcade {
    pub tiles: HashMap<Position, TileId>,
    pub process: Process,
    pub score: i64,
    pub ball_position: Option<Position>,
    pub paddle_position: Option<Position>,
}

impl Arcade {
    pub fn new() -> Self {
        Self {
            tiles: HashMap::new(),
            process: Process::new(Memory::try_from("0").unwrap(), 0, vec![]),
            score: 0,
            ball_position: None,
            paddle_position: None,
        }
    }

    pub fn count_block_tiles(&self) -> usize {
        self.tiles
            .values()
            .filter(|&value| value == &TileId::Block)
            .count()
    }

    pub fn dump_tiles(&self) {
        let minx = self.tiles.keys().map(|x| x.pos_x).min().unwrap_or(0);
        let maxx = self.tiles.keys().map(|x| x.pos_x).max().unwrap_or(0);
        let miny = self.tiles.keys().map(|x| x.pos_y).min().unwrap_or(0);
        let maxy = self.tiles.keys().map(|x| x.pos_y).max().unwrap_or(0);
        (miny..maxy + 1).for_each(|y| {
            (minx..maxx + 1).for_each(|x| {
                match self
                    .tiles
                    .get(&Position::new(x, y))
                    .unwrap_or(&TileId::Empty)
                {
                    TileId::Wall => print!("+"),
                    TileId::Block => print!("="),
                    TileId::HorizontalPaddle => print!("-"),
                    TileId::Ball => print!("o"),
                    _ => print!(" "),
                }
            });
            println!("");
        });
        println!("")
    }

    pub fn draw_output<T>(mut self, command: T) -> Result<Self, Error>
    where
        T: TryInto<DrawCommand, Error = Error> + std::fmt::Debug,
    {
        let cmd = command.try_into()?;
        match cmd.tile_id {
            TileId::Empty => {
                self.tiles.remove(&cmd.position);
            }
            TileId::Ball => {
                self.ball_position = Some(cmd.position.clone());
                self.tiles.insert(cmd.position, cmd.tile_id);
            }
            TileId::HorizontalPaddle => {
                self.paddle_position = Some(cmd.position.clone());
                self.tiles.insert(cmd.position, cmd.tile_id);
            }
            _ => {
                self.tiles.insert(cmd.position, cmd.tile_id);
            }
        };
        Ok(self)
    }

    pub fn play_for_free(self, memory: Memory) -> Result<Self, Error> {
        // Play for free
        let memory = memory.write(0, 2)?;

        let mut arcade = self;
        arcade.process = Process::new(memory, 0, vec![]);
        loop {
            let result = arcade.process.run();
            if result.is_ok() {
                arcade.process = result.unwrap();
                println!("arcade.output {:?}", arcade.process.output);
                return Ok(arcade);
            }
            arcade.process = match result {
                Ok(process) => process,
                Err(err) => match err {
                    Error::NeedInput(process) => process,
                    _ => return Err(err),
                },
            };
            let (process, output) = arcade.process.reset_output();
            arcade.process = process;
            println!(
                "arcade {} memory {}",
                arcade.tiles.len(),
                arcade.process.memory.len()
            );
            arcade = output
                .as_slice()
                .chunks(3)
                .fold(Ok(arcade), |arcade, cmd| {
                    if cmd[0] == -1 && cmd[1] == 0 {
                        println!("Score: {:?}", cmd[2]);
                        arcade
                    } else {
                        arcade.and_then(|arcade| arcade.draw_output(cmd))
                    }
                })?;
            let joystick = arcade.calculate_paddle_offset();
            arcade.process = arcade.process.with_input(vec![joystick]);
            arcade.dump_tiles();
        }
    }

    fn calculate_paddle_offset(&self) -> i64 {
        self.ball_position
            .as_ref()
            .and_then(|ball_position| {
                self.paddle_position
                    .as_ref()
                    .map(|p| (ball_position.pos_x, p.pos_x))
            })
            .map(|(ball_xpos, paddle_xpos)| {
                if ball_xpos == paddle_xpos {
                    0
                } else if ball_xpos > paddle_xpos {
                    1
                } else {
                    -1
                }
            })
            .unwrap_or(0)
    }
}
