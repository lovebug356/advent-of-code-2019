use std::convert::TryFrom;

use day13::{Arcade, Error, Memory, Process};

fn main() -> Result<(), Error> {
    println!("Care Package");

    let contents = std::fs::read_to_string("input.txt").map_err(|_err| Error::FileError)?;
    let orig_memory = Memory::try_from(contents.as_ref())?;

    let process = Process::new(orig_memory.clone(), 0, vec![]);
    let process = process.run()?;
    let arcade = Arcade::new();
    let output = &process.output;

    let arcade = output
        .as_slice()
        .chunks(3)
        .fold(Ok(arcade), |arcade, cmd| {
            arcade.and_then(|arcade| arcade.draw_output(cmd))
        })?;

    println!("Amount of block tiles: {}", arcade.count_block_tiles());

    let arcade = arcade.play_for_free(orig_memory.clone())?;

    println!("Score after all blocks broken: {}", arcade.score);

    Ok(())
}
