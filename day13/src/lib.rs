#[cfg(test)]
mod tests;

pub mod arcade;
pub mod error;
pub mod instruction;
pub mod memory;
pub mod opcode;
pub mod process;

pub use arcade::{Arcade, TileId};
pub use error::Error;
pub use memory::Memory;
pub use process::Process;
