use crate::process::Process;

#[derive(Debug)]
pub enum Error {
    ParseError,
    NeedInput(Process),
    OutOffBounds(String),
    UnknownParameterMode(String),
    ProgramFailed,
    NotFound,
    FileError,
    InvalidOpcode(String),
    InvalidTileId(String),
}
