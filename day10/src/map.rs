use std::convert::TryFrom;

use crate::Asteroid;
use crate::Error;

#[derive(Debug, Clone, PartialEq)]
pub struct AsteroidMap {
    pub asteroids: Vec<Asteroid>,
}

impl AsteroidMap {
    pub fn len(&self) -> usize {
        self.asteroids.len()
    }

    pub fn remove_asteroids(mut self, asteroids: &Vec<Asteroid>) -> Self {
        self.asteroids = self
            .asteroids
            .iter()
            .filter(|a| !asteroids.iter().any(|r| a == &r))
            .cloned()
            .collect();
        self
    }

    pub fn asteroids_in_direct_line_of_sight(&self, center: &Asteroid) -> Vec<Asteroid> {
        let mut result = self.asteroids.iter().filter(|a| a != &center).fold(
            vec![],
            |result: Vec<Asteroid>, new_asteroid| {
                let mut same_angle_found = false;
                let relative_asteroid = new_asteroid.relative_to(center);
                let mut result: Vec<_> = result
                    .iter()
                    .map(|a| {
                        let relative_in_line = a.relative_to(center);
                        if relative_in_line.has_similar_angle(&relative_asteroid) {
                            same_angle_found = true;
                            if relative_in_line.distance(center)
                                < relative_asteroid.distance(center)
                            {
                                a.clone()
                            } else {
                                new_asteroid.clone()
                            }
                        } else {
                            a.clone()
                        }
                    })
                    .collect();
                if !same_angle_found {
                    result.push(new_asteroid.clone())
                }
                result
            },
        );
        result.sort_by(|a, b| a.relative_to(center).cmp(&b.relative_to(center)));
        result
    }

    pub fn search_center_max_line_of_sight(&self) -> Result<Asteroid, Error> {
        Ok(self
            .asteroids
            .iter()
            .map(|a| (a, self.asteroids_in_direct_line_of_sight(a)))
            .max_by_key(|x| x.1.len())
            .map(|x| x.0.clone())
            .ok_or(Error::NoAsteroidsFound)?)
    }

    pub fn calculate_vaporization_order(&self, center: &Asteroid) -> Vec<Asteroid> {
        let mut clone = self.clone();
        let mut result = vec![];
        loop {
            let mut round = clone.asteroids_in_direct_line_of_sight(center);
            clone = clone.remove_asteroids(&round);
            result.append(&mut round);
            if clone.len() <= 1 {
                break;
            }
        }
        result
    }
}

impl TryFrom<&str> for AsteroidMap {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let asteroids = value
            .trim()
            .split("\n")
            .filter(|line| line != &"")
            .enumerate()
            .flat_map(|(ypos, line)| {
                line.trim()
                    .chars()
                    .enumerate()
                    .filter_map(move |(xpos, c)| match c {
                        '#' => Some(Asteroid::new(xpos as i32, ypos as i32)),
                        _ => None,
                    })
            })
            .collect::<Vec<_>>();
        Ok(Self { asteroids })
    }
}
