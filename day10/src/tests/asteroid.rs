use crate::{Asteroid, Error};
use std::f32;

#[test]
fn calculate_distance() -> Result<(), Error> {
    let a1 = Asteroid::new(0, 0);

    assert_eq!(a1.distance(&Asteroid::new(0, 0)), 0f32);
    assert_eq!(a1.distance(&Asteroid::new(0, 1)), 1f32);
    assert_eq!(a1.distance(&Asteroid::new(2, 0)), 2f32);
    assert_eq!(a1.distance(&Asteroid::new(0, 3)), 3f32);
    assert_eq!(a1.distance(&Asteroid::new(3, 4)), 5f32);

    Ok(())
}

#[test]
fn relative_to() {
    let center = Asteroid::new(1, 1);
    assert_eq!(
        Asteroid::new(0, 0).relative_to(&center),
        Asteroid::new(-1, -1)
    )
}

#[test]
fn similar_angle() -> Result<(), Error> {
    assert_eq!(
        Asteroid::new(0, 0).has_similar_angle(&Asteroid::new(0, 0)),
        false
    );
    assert_eq!(
        Asteroid::new(0, 0).has_similar_angle(&Asteroid::new(0, 1)),
        false
    );
    assert_eq!(
        Asteroid::new(0, 0).has_similar_angle(&Asteroid::new(1, 0)),
        false
    );
    assert_eq!(
        Asteroid::new(1, 0).has_similar_angle(&Asteroid::new(0, 0)),
        false
    );
    assert_eq!(
        Asteroid::new(0, 1).has_similar_angle(&Asteroid::new(0, 0)),
        false
    );

    assert_eq!(
        Asteroid::new(0, 1).has_similar_angle(&Asteroid::new(0, 1)),
        true
    );
    assert_eq!(
        Asteroid::new(1, 1).has_similar_angle(&Asteroid::new(1, 1)),
        true
    );
    assert_eq!(
        Asteroid::new(1, 1).has_similar_angle(&Asteroid::new(2, 2)),
        true
    );

    assert_eq!(
        Asteroid::new(1, 1).has_similar_angle(&Asteroid::new(2, 3)),
        false
    );
    assert_eq!(
        Asteroid::new(1, 1).has_similar_angle(&Asteroid::new(-1, -1)),
        false
    );

    Ok(())
}

#[test]
fn calculate_angle() -> Result<(), Error> {
    assert_eq!(Asteroid::new(0, -1).angle(), 0f32);
    assert_eq!(Asteroid::new(1, 0).angle(), f32::consts::FRAC_PI_2);
    assert_eq!(Asteroid::new(0, 1).angle(), f32::consts::PI);
    assert_eq!(Asteroid::new(-1, 0).angle(), f32::consts::PI * 3f32 / 2f32);
    assert_eq!(Asteroid::new(1, -1).angle(), f32::consts::PI / 4f32);
    assert_eq!(Asteroid::new(1, 1).angle(), f32::consts::PI * 3f32 / 4f32);
    assert_eq!(Asteroid::new(-1, -1).angle(), f32::consts::PI * 7f32 / 4f32);

    Ok(())
}

#[test]
fn asteroid_ordening() -> Result<(), Error> {
    assert!(Asteroid::new(0, -1) < Asteroid::new(1, 0));
    assert!(Asteroid::new(1, 0) > Asteroid::new(0, -1));
    assert!(Asteroid::new(0, -1) < Asteroid::new(-1, -1));
    assert!(Asteroid::new(1, -1) < Asteroid::new(-1, -1));

    Ok(())
}
