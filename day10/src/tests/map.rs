use std::convert::TryFrom;

use crate::{Asteroid, AsteroidMap, Error};

#[test]
fn asteroid_map_from_str() -> Result<(), Error> {
    let map = AsteroidMap::try_from(".")?;
    assert_eq!(map.len(), 0);

    let map = AsteroidMap::try_from("#")?;
    assert_eq!(map.len(), 1);
    assert_eq!(map.asteroids[0], Asteroid::new(0, 0));

    let map = AsteroidMap::try_from(".#")?;
    assert_eq!(map.len(), 1);
    assert_eq!(map.asteroids[0], Asteroid::new(1, 0));

    let map = AsteroidMap::try_from(".#.#")?;
    assert_eq!(map.len(), 2);
    assert_eq!(map.asteroids[0], Asteroid::new(1, 0));
    assert_eq!(map.asteroids[1], Asteroid::new(3, 0));

    let map = AsteroidMap::try_from(".\n#")?;
    assert_eq!(map.len(), 1);
    assert_eq!(map.asteroids[0], Asteroid::new(0, 1));

    let map = AsteroidMap::try_from(".#.\n..#")?;
    assert_eq!(map.len(), 2);
    assert_eq!(map.asteroids[0], Asteroid::new(1, 0));
    assert_eq!(map.asteroids[1], Asteroid::new(2, 1));

    // From Example
    let map = ".#..#\n.....\n#####\n....#\n...##";
    let map = AsteroidMap::try_from(map)?;
    assert_eq!(map.len(), 10);
    assert_eq!(
        map.asteroids_in_direct_line_of_sight(&map.asteroids[0])
            .len(),
        7
    );
    assert_eq!(
        map.asteroids_in_direct_line_of_sight(&map.asteroids[1])
            .len(),
        7
    );
    assert_eq!(
        map.asteroids_in_direct_line_of_sight(&map.asteroids[2])
            .len(),
        6
    );
    assert_eq!(map.search_center_max_line_of_sight()?, Asteroid::new(3, 4));

    let map = "
......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####
";
    let map = AsteroidMap::try_from(map)?;
    let result = map.search_center_max_line_of_sight()?;
    assert_eq!(result, Asteroid::new(5, 8));
    assert_eq!(map.asteroids_in_direct_line_of_sight(&result).len(), 33);

    let map = "
#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.
";
    let map = AsteroidMap::try_from(map)?;
    let result = map.search_center_max_line_of_sight()?;
    assert_eq!(result, Asteroid::new(1, 2));
    assert_eq!(map.asteroids_in_direct_line_of_sight(&result).len(), 35);

    let map = "
.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..
";
    let map = AsteroidMap::try_from(map)?;
    let result = map.search_center_max_line_of_sight()?;
    assert_eq!(result, Asteroid::new(6, 3));
    assert_eq!(map.asteroids_in_direct_line_of_sight(&result).len(), 41);

    let map = "
.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##
";
    let map = AsteroidMap::try_from(map)?;
    let result = map.search_center_max_line_of_sight()?;
    assert_eq!(result, Asteroid::new(11, 13));
    assert_eq!(map.asteroids_in_direct_line_of_sight(&result).len(), 210);

    Ok(())
}

#[test]
fn asteroids_in_direct_line_of_sight() -> Result<(), Error> {
    let map = AsteroidMap::try_from("#.\n##")?;
    let asteroids = map.asteroids_in_direct_line_of_sight(&Asteroid::new(0, 1));
    assert_eq!(asteroids.len(), 2);
    assert_eq!(asteroids[0], Asteroid::new(0, 0));
    assert_eq!(asteroids[1], Asteroid::new(1, 1));

    let map = AsteroidMap::try_from("#.\n#.\n#.")?;
    let asteroids = map.asteroids_in_direct_line_of_sight(&Asteroid::new(0, 2));
    assert_eq!(asteroids.len(), 1);
    assert_eq!(asteroids[0], Asteroid::new(0, 1));

    Ok(())
}

#[test]
fn remove_asteroids() -> Result<(), Error> {
    let map = AsteroidMap::try_from("#")?;
    let map = map.remove_asteroids(&vec![Asteroid::new(0, 0)]);
    assert_eq!(map.len(), 0);

    let map = AsteroidMap::try_from("##")?;
    let map = map.remove_asteroids(&vec![Asteroid::new(0, 0)]);
    assert_eq!(map.len(), 1);
    assert_eq!(map.asteroids[0], Asteroid::new(1, 0));

    Ok(())
}

#[test]
fn vaporization_order() -> Result<(), Error> {
    let map = AsteroidMap::try_from("#..\n###")?;
    let order = map.calculate_vaporization_order(&Asteroid::new(0, 1));

    assert_eq!(order.len(), 3);
    assert_eq!(order[0], Asteroid::new(0, 0));
    assert_eq!(order[1], Asteroid::new(1, 1));
    assert_eq!(order[2], Asteroid::new(2, 1));

    let map = AsteroidMap::try_from("#..\n####")?;
    let order = map.calculate_vaporization_order(&Asteroid::new(0, 1));

    assert_eq!(order.len(), 4);
    assert_eq!(order[3], Asteroid::new(3, 1));

    let map = AsteroidMap::try_from("###\n.#.")?;
    let order = map.calculate_vaporization_order(&Asteroid::new(1, 1));

    assert_eq!(order.len(), 3);
    assert_eq!(order[0], Asteroid::new(1, 0));
    assert_eq!(order[1], Asteroid::new(2, 0));
    assert_eq!(order[2], Asteroid::new(0, 0));

    Ok(())
}
