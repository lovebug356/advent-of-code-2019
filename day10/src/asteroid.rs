use std::cmp::Ordering;
use std::f32;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Asteroid {
    xpos: i32,
    ypos: i32,
}

#[derive(Debug, Clone)]
pub struct Angle {
    xpos: i32,
    ypos: i32,
}

impl Asteroid {
    pub fn new(xpos: i32, ypos: i32) -> Self {
        Self { xpos, ypos }
    }

    pub fn normal() -> Self {
        Self { xpos: 1, ypos: 0 }
    }

    pub fn relative_to(&self, center: &Self) -> Self {
        Self {
            xpos: self.xpos - center.xpos,
            ypos: self.ypos - center.ypos,
        }
    }

    pub fn dot(&self, peer: &Self) -> f32 {
        self.xpos as f32 * peer.xpos as f32 + self.ypos as f32 * peer.ypos as f32
    }

    pub fn magnitude(&self) -> f32 {
        ((self.xpos as f32).powi(2) + (self.ypos as f32).powi(2)).sqrt()
    }

    pub fn distance(&self, peer: &Self) -> f32 {
        self.relative_to(peer).magnitude()
    }

    pub fn has_similar_angle(&self, peer: &Self) -> bool {
        if self.magnitude() == 0f32 || peer.magnitude() == 0f32 {
            return false;
        }

        if self.xpos * peer.xpos < 0 || self.ypos * peer.ypos < 0 {
            return false;
        }

        return (self.xpos as f32 / self.ypos as f32) == (peer.xpos as f32 / peer.ypos as f32);
    }

    pub fn angle(&self) -> f32 {
        if self.xpos == 0 {
            if self.ypos > 0 {
                f32::consts::PI
            } else {
                0f32
            }
        } else {
            if self.ypos == 0 {
                if self.xpos > 0 {
                    f32::consts::FRAC_PI_2
                } else {
                    f32::consts::FRAC_PI_2 * 3f32
                }
            } else {
                let result = (-1f32 * self.xpos as f32 / self.ypos as f32).atan();
                if self.ypos > 0 {
                    f32::consts::PI + result
                } else {
                    if self.xpos < 0 {
                        f32::consts::PI * 2f32 + result
                    } else {
                        result
                    }
                }
            }
        }
    }
}

impl PartialOrd for Asteroid {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Asteroid {
    fn cmp(&self, other: &Self) -> Ordering {
        let a1 = self.angle();
        let a2 = other.angle();
        if a1 == a2 {
            Ordering::Equal
        } else if a1 < a2 {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    }
}
