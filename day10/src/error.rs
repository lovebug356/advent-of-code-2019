#[derive(Debug, Clone)]
pub enum Error {
    ParsePositionFailed(String),
    NoAsteroidsFound,
    FileError,
}
