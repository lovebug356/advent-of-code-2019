use std::convert::TryFrom;

use day10::AsteroidMap;
use day10::Error;

fn main() -> Result<(), Error> {
    let contents = std::fs::read_to_string("input.txt").map_err(|_err| Error::FileError)?;
    let map = AsteroidMap::try_from(contents.as_ref())?;
    let result = map.search_center_max_line_of_sight()?;

    println!("Monitoring Station");
    println!("Center asteroid: {:?}", result);
    println!(
        "Asteroids in direct line of sight: {}",
        map.asteroids_in_direct_line_of_sight(&result).len()
    );

    println!("Phase 2:");
    let order = map.calculate_vaporization_order(&result);
    println!("the 200th asteroid to be vaporized: {:?}", order[199]);

    Ok(())
}
