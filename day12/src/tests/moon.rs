use crate::Moon;

#[test]
fn apply_velocity() {
    let moon = Moon::default()
        .with_position([1, 2, 3])
        .with_velocity([-2, 0, 3]);

    let moon = moon.apply_velocity();
    assert_eq!(moon.position, [-1, 2, 6]);
}

#[test]
fn apply_gravity() {
    let moon_1 = Moon::default().with_position([1, 2, 3]);
    let moon_2 = Moon::default().with_position([3, 1, 2]);

    let moon_1 = moon_1.apply_gravity(&moon_2);
    assert_eq!(moon_1.velocity, [1, -1, -1]);

    let moon_2 = moon_2.apply_gravity(&moon_1);
    assert_eq!(moon_2.velocity, [-1, 1, 1]);
}

#[test]
fn calculate_energy() {
    let moon = Moon::default()
        .with_position([2, 1, -3])
        .with_velocity([-3, -2, 1]);
    assert_eq!(moon.potential_energy(), 6);
    assert_eq!(moon.kinetic_energy(), 6);
    assert_eq!(moon.total_energy(), 36);
}
