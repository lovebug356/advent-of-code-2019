use crate::{Jupiter, Moon};

#[test]
fn example() {
    let jupiter = Jupiter::new()
        .add_moon(Moon::default().with_position([-1, 0, 2]))
        .add_moon(Moon::default().with_position([2, -10, -7]))
        .add_moon(Moon::default().with_position([4, -8, 8]))
        .add_moon(Moon::default().with_position([3, 5, -1]));

    let jupiter = jupiter.apply_time_step();
    assert_eq!(jupiter.moons[0].velocity, [3, -1, -1]);
    assert_eq!(jupiter.moons[0].position, [2, -1, 1]);

    let jupiter = jupiter.apply_time_step();
    assert_eq!(jupiter.moons[0].velocity, [3, -2, -2]);
    assert_eq!(jupiter.moons[0].position, [5, -3, -1]);
}
