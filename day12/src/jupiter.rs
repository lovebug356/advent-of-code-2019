use crate::Moon;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Jupiter {
    pub moons: Vec<Moon>,
}

impl Jupiter {
    pub fn new() -> Self {
        Self { moons: vec![] }
    }

    pub fn add_moon(mut self, moon: Moon) -> Self {
        self.moons.push(moon);
        self
    }

    pub fn total_energy(&self) -> u32 {
        self.moons
            .iter()
            .fold(0, |result, moon| result + moon.total_energy())
    }

    pub fn apply_time_step(mut self) -> Self {
        let new_moons: Vec<Moon> = self
            .moons
            .iter()
            .map(|moon| {
                self.moons
                    .iter()
                    .fold(moon.clone(), |center, moon| center.apply_gravity(moon))
                    .apply_velocity()
            })
            .collect::<Vec<_>>();
        self.moons = new_moons;
        self
    }

    pub fn search_cycle(&self) -> i64 {
        (0..3).fold(1, |result, axis| {
            least_common_denominator(result, self.search_cycle_single_axis(axis))
        })
    }

    pub fn search_cycle_single_axis(&self, idx: usize) -> i64 {
        let mut counter = 0;
        let mut state = self.clone();

        loop {
            state = state.apply_time_step();
            counter += 1;
            if self.equal_axis(&state, idx) {
                break;
            }
        }

        counter
    }

    pub fn equal_axis(&self, peer: &Self, axis: usize) -> bool {
        self.moons.iter().zip(peer.moons.iter()).all(|(m1, m2)| {
            m1.position[axis] == m2.position[axis] && m1.velocity[axis] == m2.velocity[axis]
        })
    }
}

fn least_common_denominator(a: i64, b: i64) -> i64 {
    let mut params = if a >= b { (a, b) } else { (b, a) };
    while params.1 != 0 {
        params = (params.1, params.0 % params.1);
    }
    (a * b) / params.0
}
