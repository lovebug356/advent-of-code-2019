use day12::{Jupiter, Moon};

fn main() {
    let jupiter_state_zero = Jupiter::new()
        .add_moon(Moon::default().with_position([17, -12, 13]))
        .add_moon(Moon::default().with_position([2, 1, 1]))
        .add_moon(Moon::default().with_position([-1, -17, 7]))
        .add_moon(Moon::default().with_position([12, -14, 18]));

    let jupiter = (0..1000).fold(jupiter_state_zero.clone(), |jupiter, _idx| {
        jupiter.apply_time_step()
    });

    println!("Total energy around jupiter: {}", jupiter.total_energy());

    let jupiter = jupiter_state_zero.clone();
    println!("Search repeat: {}", jupiter.search_cycle());
}
