use std::cmp::Ordering;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Moon {
    pub position: [i32; 3],
    pub velocity: [i32; 3],
}

impl Moon {
    pub fn default() -> Self {
        Self {
            position: [0i32; 3],
            velocity: [0i32; 3],
        }
    }

    pub fn with_position(mut self, position: [i32; 3]) -> Self {
        self.position = position;
        self
    }

    pub fn with_velocity(mut self, velocity: [i32; 3]) -> Self {
        self.velocity = velocity;
        self
    }

    pub fn potential_energy(&self) -> u32 {
        (self.position[0].abs() + self.position[1].abs() + self.position[2].abs()) as u32
    }

    pub fn kinetic_energy(&self) -> u32 {
        (self.velocity[0].abs() + self.velocity[1].abs() + self.velocity[2].abs()) as u32
    }

    pub fn total_energy(&self) -> u32 {
        self.potential_energy() * self.kinetic_energy()
    }

    pub fn apply_velocity(self) -> Self {
        let new_position = [
            self.position[0] + self.velocity[0],
            self.position[1] + self.velocity[1],
            self.position[2] + self.velocity[2],
        ];
        self.with_position(new_position)
    }

    pub fn apply_gravity(mut self, peer: &Self) -> Self {
        for idx in 0..3 {
            let offset = match self.position[idx].cmp(&peer.position[idx]) {
                Ordering::Less => 1,
                Ordering::Greater => -1,
                _ => 0,
            };
            self.velocity[idx] += offset;
        }
        self
    }
}
