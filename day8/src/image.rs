use crate::Error;
use crate::Layer;

pub struct Image {
    pub width: usize,
    pub height: usize,
    pub layers: Vec<Layer>,
}

impl Image {
    pub fn new(width: usize, height: usize, data: &str) -> Result<Self, Error> {
        let layer_size = width * height;
        let layers = data
            .as_bytes()
            .chunks(layer_size)
            .map(|c| std::str::from_utf8(c).unwrap())
            .filter(|chunk| chunk != &"\n")
            .map(|chunk| Layer::new(width, height, chunk))
            .collect::<Result<Vec<_>, _>>()?;
        Ok(Self {
            width,
            height,
            layers,
        })
    }

    pub fn search_fewest_digits(&self, digit: u32) -> Result<usize, Error> {
        self.layers
            .iter()
            .enumerate()
            .map(|(idx, layer)| (idx, layer.count_digit(digit)))
            .min_by(|x, y| x.1.cmp(&y.1))
            .map(|(idx, _count)| idx)
            .ok_or(Error::DigitNotFound)
    }

    pub fn apply_layers(&self) -> Result<Layer, Error> {
        let result = (0..self.width * self.height)
            .map(|p| {
                self.layers
                    .iter()
                    .map(|l| l.digits[p])
                    .find(|&x| {
                        if p == 2 {
                            println!("{}", x);
                        }
                        x != 2
                    })
                    .unwrap_or(0)
            })
            .collect::<Vec<u32>>();
        Ok(Layer {
            width: self.width,
            height: self.height,
            digits: result,
        })
    }
}
