#[derive(Debug)]
pub enum Error {
    FileError,
    ParseError,
    DigitNotFound,
}
