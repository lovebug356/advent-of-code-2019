use crate::{Error, Image};

#[test]
fn image_from_string() -> Result<(), Error> {
    let image = Image::new(3, 2, "123456789012")?;

    assert_eq!(image.layers.len(), 2);

    Ok(())
}

#[test]
fn search_fewest_digits() -> Result<(), Error> {
    let image = Image::new(2, 2, "022301011212")?;

    assert_eq!(image.search_fewest_digits(0)?, 2);
    assert_eq!(image.search_fewest_digits(1)?, 0);

    Ok(())
}

#[test]
fn apply_layers() -> Result<(), Error> {
    let image = Image::new(2, 2, "00001111")?;
    assert_eq!(image.apply_layers()?.message(), "  \n  \n");

    let image = Image::new(3, 2, "011011")?;
    assert_eq!(image.apply_layers()?.message(), " xx\n xx\n");

    let image = Image::new(2, 2, "11110000")?;
    assert_eq!(image.apply_layers()?.message(), "xx\nxx\n");

    let image = Image::new(2, 2, "12120000")?;
    assert_eq!(image.apply_layers()?.message(), "x \nx \n");

    let image = Image::new(2, 2, "222222221010")?;
    assert_eq!(image.apply_layers()?.message(), "x \nx \n");

    let image = Image::new(2, 2, "0222112222120000")?;
    assert_eq!(image.apply_layers()?.message(), " x\nx \n");

    Ok(())
}
