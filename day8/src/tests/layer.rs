use crate::{Error, Layer};

#[test]
fn count_digit() -> Result<(), Error> {
    let layer = Layer::new(3, 2, "123456")?;
    assert_eq!(layer.count_digit(1), 1);
    assert_eq!(layer.count_digit(2), 1);

    let layer = Layer::new(3, 2, "111223")?;
    assert_eq!(layer.count_digit(1), 3);
    assert_eq!(layer.count_digit(2), 2);
    assert_eq!(layer.count_digit(3), 1);
    assert_eq!(layer.count_digit(4), 0);

    Ok(())
}
