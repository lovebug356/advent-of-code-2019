use crate::Error;

#[derive(Debug, Clone, PartialEq)]
pub struct Layer {
    pub width: usize,
    pub height: usize,
    pub digits: Vec<u32>,
}

impl Layer {
    pub fn new(width: usize, height: usize, data: &str) -> Result<Self, Error> {
        println!("width: {}, height, {}, data {:?}", width, height, data);
        let digits = data
            .chars()
            .map(|x| x.to_digit(10))
            .collect::<Option<Vec<_>>>()
            .ok_or(Error::ParseError)?;
        Ok(Self {
            width,
            height,
            digits,
        })
    }

    pub fn count_digit(&self, digit: u32) -> usize {
        self.digits.iter().filter(|&x| x == &digit).count()
    }

    pub fn message(&self) -> String {
        let mut result = Vec::new();
        for idx in 0..self.height {
            for idx2 in 0..self.width {
                match self.digits[idx * self.width + idx2] {
                    0 => result.push(" ".to_string()),
                    _ => result.push("x".to_string()),
                }
            }
            result.push("\n".to_string());
        }
        result.join("")
    }
}
