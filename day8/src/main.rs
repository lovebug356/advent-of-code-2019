use day8::{Error, Image};

fn main() -> Result<(), Error> {
    let contents = std::fs::read_to_string("input.txt").map_err(|_err| Error::FileError)?;

    let image = Image::new(25, 6, &contents)?;
    let layer_idx = image.search_fewest_digits(0)?;
    println!("Selected layer: {}", layer_idx);

    let layer = &image.layers[layer_idx];
    let result = layer.count_digit(1) * layer.count_digit(2);
    println!("Phase 1: {}", result);

    println!("Message:\n{}", image.apply_layers()?.message());

    Ok(())
}
