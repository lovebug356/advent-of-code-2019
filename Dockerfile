FROM archlinux/base
MAINTAINER Thijs Vermeir <thijsvermeir@gmail.com>

# Update base image.
RUN pacman-key --refresh-keys
RUN pacman -Syu --noconfirm
RUN pacman-db-upgrade

RUN pacman -Syu --noconfirm rust base-devel
