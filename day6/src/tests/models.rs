use crate::models::OrbitObject;

#[test]
fn create_object() {
    let o1 = OrbitObject::new("ABC");
    assert_eq!(o1.name, "ABC");
    assert_eq!(o1.center, None);
    assert_eq!(o1.planets, Vec::<String>::new());

    let o2 = o1.with_center("DEF");
    assert_eq!(o2.name, "ABC");
    assert_eq!(o2.center, Some("DEF".to_string()));
    assert_eq!(o2.planets, Vec::<String>::new());

    let o3 = o2.add_planet("GHI");
    assert_eq!(o3.name, "ABC");
    assert_eq!(o3.center, Some("DEF".to_string()));
    assert_eq!(o3.planets, vec!["GHI"]);

    let o4 = o3.add_planet("JKL");
    assert_eq!(o4.planets, vec!["GHI", "JKL"]);
}

#[test]
fn has_planet() {
    let o1 = OrbitObject::new("ABC").add_planet("DEF");

    assert!(o1.has_planet("DEF"));
}
