use crate::map::OrbitMap;

#[test]
fn create_map_and_add_orbit() {
    let map = OrbitMap::default();

    let map: OrbitMap = map.add_orbit("ABC", "DEF");

    assert!(map.get("ABC").is_some());
}

#[test]
fn from_str() {
    let map = OrbitMap::from("COM)BAR");

    assert!(map.get("COM").is_some());
    assert!(map.get("BAR").is_some());

    let map = OrbitMap::from("COM)BAR\nCOM)FOO");

    assert!(map.get("COM").is_some());
    assert!(map.get("BAR").is_some());
    assert!(map.get("FOO").is_some());
}

#[test]
fn distance() {
    let map = OrbitMap::from("FOO)BAR");

    assert_eq!(map.distance("FOO", "BAR"), Some(1));
    assert_eq!(map.distance("BAR", "FOO"), None);

    let map = OrbitMap::from("COM)FOO\nFOO)BAR");

    assert_eq!(map.distance("FOO", "BAR"), Some(1));
    assert_eq!(map.distance("BAR", "FOO"), None);
    assert_eq!(map.distance("COM", "BAR"), Some(2));
    assert_eq!(map.distance("COM", "THIS"), None);
}

#[test]
fn total_orbits() {
    let map = OrbitMap::from("FOO)BAR");
    assert_eq!(map.total_orbits("FOO"), Some(1));

    let map = OrbitMap::from("FOO)BAR1\nFOO)BAR2");
    assert_eq!(map.total_orbits("FOO"), Some(2));

    let map = OrbitMap::from("FOO)BAR1\nBAR1)BAR2");
    assert_eq!(map.total_orbits("FOO"), Some(3));

    let map = OrbitMap::from("FOO)BAR1\nBAR1)BAR2\nBAR2)BAR3");
    assert_eq!(map.total_orbits("FOO"), Some(6));

    let map = OrbitMap::from(
        "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L",
    );
    assert_eq!(map.total_orbits("COM"), Some(42));
}
