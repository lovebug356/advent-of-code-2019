#[derive(Debug, PartialEq, Clone)]
pub struct OrbitObject {
    pub name: String,
    pub center: Option<String>,
    pub planets: Vec<String>,
}

impl OrbitObject {
    pub fn new<S: Into<String>>(name: S) -> OrbitObject {
        OrbitObject {
            name: name.into(),
            center: None,
            planets: vec![],
        }
    }

    pub fn with_center<S: Into<String>>(mut self, center: S) -> OrbitObject {
        self.center = Some(center.into());
        self
    }

    pub fn add_planet<S: Into<String>>(mut self, planet: S) -> OrbitObject {
        self.planets.push(planet.into());
        self
    }

    pub fn has_planet<S: AsRef<str>>(&self, planet: S) -> bool {
        self.planets
            .iter()
            .find(|p| p == &planet.as_ref())
            .is_some()
    }
}
