use std::collections::HashMap;

use crate::models::OrbitObject;

#[derive(Debug, Default)]
pub struct OrbitMap {
    pub objects: HashMap<String, OrbitObject>,
}

impl OrbitMap {
    pub fn get<S>(&self, name: S) -> Option<&OrbitObject>
    where
        S: AsRef<str>,
    {
        self.objects.get(name.as_ref())
    }

    pub fn add_orbit<S1, S2>(mut self, center_name: S1, planet_name: S2) -> OrbitMap
    where
        S1: Into<String> + AsRef<str> + Copy,
        S2: Into<String> + AsRef<str> + Copy,
    {
        let center_object = self
            .objects
            .remove(center_name.as_ref())
            .unwrap_or_else(|| OrbitObject::new(center_name.as_ref()))
            .add_planet(planet_name.into());
        let planet_object = self
            .objects
            .remove(planet_name.as_ref())
            .unwrap_or_else(|| OrbitObject::new(planet_name))
            .with_center(center_name);
        self.objects.insert(center_name.into(), center_object);
        self.objects.insert(planet_name.into(), planet_object);
        self
    }

    pub fn total_orbits<S: Into<String>>(&self, center_str: S) -> Option<usize> {
        std::iter::successors(
            Some((vec![center_str.into()], 0usize)),
            |(center_names, offset)| {
                if center_names.len() > 0 {
                    Some((
                        center_names
                            .iter()
                            .flat_map(|name| {
                                self.objects
                                    .get(name)
                                    .map(|center| center.planets.iter().cloned())
                            })
                            .flat_map(|x| x)
                            .collect(),
                        offset + 1,
                    ))
                } else {
                    None
                }
            },
        )
        .fold(None, |result, (names, offset)| {
            Some(result.unwrap_or(0) + names.len() * offset)
        })
    }

    pub fn distance<S1, S2>(&self, center_str: S1, planet_str: S2) -> Option<usize>
    where
        S1: AsRef<str>,
        S2: Into<String>,
    {
        std::iter::successors(Some(&planet_str.into()), |&name| {
            self.center_of_object_with_name(name)
        })
        .enumerate()
        .find(|(_idx, name)| name == &center_str.as_ref())
        .map(|(idx, _name)| idx)
    }

    fn center_of_object_with_name<S: AsRef<str>>(&self, name: S) -> Option<&String> {
        self.objects
            .get(name.as_ref())
            .map(|center| center.center.as_ref())
            .unwrap_or(None)
    }
}

impl From<&str> for OrbitMap {
    fn from(value: &str) -> OrbitMap {
        value
            .trim()
            .split("\n")
            .fold(OrbitMap::default(), |map, line| {
                let object_str = line.trim().split(")").collect::<Vec<&str>>();
                map.add_orbit(object_str[0], object_str[1])
            })
    }
}
