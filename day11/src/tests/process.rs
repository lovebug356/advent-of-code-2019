use std::convert::TryFrom;

use crate::memory::Memory;
use crate::process::Process;
use crate::Error;

#[test]
fn from_memory() {
    let memory = Memory::try_from("1,2,3").unwrap();
    let process = Process::from(memory);

    assert_eq!(process.pointer, 0);
}

#[test]
fn process_run() -> Result<(), Error> {
    let memory = Memory::try_from("1101,100,-1,4,0")?;
    let process = Process::from(memory);
    let memory = process.run()?.memory;
    assert_eq!(memory.read(4)?, 99);

    let memory = Memory::try_from("101,100,1,5,99,0")?;
    let process = Process::from(memory);
    let memory = process.run()?.memory;
    assert_eq!(memory.read(5)?, 200);

    let memory = Memory::try_from("1102,2,3,5,99,0")?;
    let process = Process::from(memory);
    let memory = process.run()?.memory;
    assert_eq!(memory.read(5)?, 6);

    let memory = Memory::try_from("1,0,0,0,99")?;
    let computer = Process::from(memory);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(0)?, 2);

    let memory = Memory::try_from("2,3,0,3,99")?;
    let computer = Process::from(memory);
    let memory = computer.run()?.memory;
    assert_eq!(memory.len(), 5);
    assert_eq!(memory.read(3)?, 6);

    let memory = Memory::try_from("2,4,4,5,99,0")?;
    let computer = Process::from(memory);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(5)?, 9801);

    let memory = Memory::try_from("1,1,1,4,99,5,6,0,99")?;
    let computer = Process::from(memory);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(0)?, 30);
    assert_eq!(memory.read(4)?, 2);

    let memory = Memory::try_from("3,9,8,9,10,9,4,9,99,-1,8")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![0]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(9)?, 0);

    let memory = Memory::try_from("3,9,8,9,10,9,4,9,99,-1,8")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![8]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(9)?, 1);

    let memory = Memory::try_from("3,9,7,9,10,9,4,9,99,-1,8")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![8]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(9)?, 0);

    let memory = Memory::try_from("3,9,7,9,10,9,4,9,99,-1,8")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![7]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(9)?, 1);

    let memory = Memory::try_from("3,3,1108,-1,8,3,4,3,99")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![8]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(3)?, 1);

    let memory = Memory::try_from("3,3,1108,-1,8,3,4,3,99")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![7]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(3)?, 0);

    let memory = Memory::try_from("3,3,1107,-1,8,3,4,3,99")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![7]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(3)?, 1);

    let memory = Memory::try_from("3,3,1107,-1,8,3,4,3,99")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![8]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(3)?, 0);

    let memory = Memory::try_from("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![0]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(13)?, 0);

    let memory = Memory::try_from("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(vec![1]);
    let memory = computer.run()?.memory;
    assert_eq!(memory.read(13)?, 1);

    let memory = Memory::try_from("109,19,99")?;
    let computer = Process::from(memory).with_relative_base(2000);
    let computer = computer.run()?;
    assert_eq!(computer.relative_base, 2019);

    let orig_memory =
        Memory::try_from("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99")?;
    let computer = Process::from(orig_memory.clone());
    let memory = computer.run()?.memory;
    for idx in 0..memory.len() {
        assert_eq!(memory.read(idx)?, orig_memory.read(idx)?);
    }

    let orig_memory = Memory::try_from("1102,34915192,34915192,7,4,7,99,0")?;
    let computer = Process::from(orig_memory.clone());
    let _memory = computer.run()?.memory;

    let orig_memory = Memory::try_from("104,1125899906842624,99")?;
    let computer = Process::from(orig_memory.clone());
    let computer = computer.run()?;
    assert_eq!(computer.output[0], 1125899906842624);

    Ok(())
}

#[test]
fn test_save_with_relative_base() -> Result<(), Error> {
    let memory = Memory::try_from("109,2,203,0,99")?;
    let computer = Process::from(memory).with_single_input(1);
    let computer = computer.run()?;
    assert_eq!(computer.relative_base, 2);
    assert_eq!(computer.memory.read(2)?, 1);

    Ok(())
}
