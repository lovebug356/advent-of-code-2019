use crate::Orientation;
use crate::Turn;

#[test]
fn rotate() {
    assert_eq!(Orientation::Up.rotate(Turn::Right), Orientation::Right);
    assert_eq!(Orientation::Left.rotate(Turn::Right), Orientation::Up);
    assert_eq!(Orientation::Up.rotate(Turn::Left), Orientation::Left);
}
