use std::convert::TryFrom;

use crate::opcode::Opcode;
use crate::Error;

#[test]
fn from_i32() -> Result<(), Error> {
    assert_eq!(Opcode::try_from(1)?, Opcode::Add);
    assert_eq!(Opcode::try_from(2)?, Opcode::Multiply);
    assert_eq!(Opcode::try_from(3)?, Opcode::Save);
    assert_eq!(Opcode::try_from(4)?, Opcode::Output);
    assert_eq!(Opcode::try_from(9)?, Opcode::AdjustRelativeBase);
    assert_eq!(Opcode::try_from(99)?, Opcode::EndOfProgram);

    Ok(())
}

#[test]
fn size() -> Result<(), Error> {
    assert_eq!(Opcode::Add.size(), 4);
    assert_eq!(Opcode::Multiply.size(), 4);
    assert_eq!(Opcode::Save.size(), 2);
    assert_eq!(Opcode::Output.size(), 2);
    assert_eq!(Opcode::AdjustRelativeBase.size(), 2);
    assert_eq!(Opcode::EndOfProgram.size(), 1);

    Ok(())
}
