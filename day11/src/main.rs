use std::convert::TryFrom;

use day11::{Error, HullPaintingRobot, Memory};

fn main() -> Result<(), Error> {
    let contents = std::fs::read_to_string("input.txt").map_err(|_err| Error::FileError)?;
    let memory = Memory::try_from(contents.as_ref())?;

    println!("Space Police");
    let robot = HullPaintingRobot::from(memory.clone());
    let robot = robot.run()?;
    robot.dump_message();

    println!("Painted panels: {}", robot.painted_panels());

    let robot = HullPaintingRobot::from(memory).paint_white();
    let robot = robot.run()?;
    robot.dump_message();

    Ok(())
}
