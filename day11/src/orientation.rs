#[derive(Debug, Clone)]
pub enum Turn {
    Left,
    Right,
}

impl From<i64> for Turn {
    fn from(value: i64) -> Self {
        match value {
            0 => Turn::Left,
            _ => Turn::Right,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Orientation {
    Up,
    Right,
    Down,
    Left,
}

impl Orientation {
    pub fn rotate(&self, turn: Turn) -> Self {
        let offset = match turn {
            Turn::Left => -1,
            Turn::Right => 1,
        };
        Orientation::from(i32::from(self) + offset)
    }
}

impl From<&Orientation> for i32 {
    fn from(value: &Orientation) -> i32 {
        match value {
            Orientation::Up => 0,
            Orientation::Right => 1,
            Orientation::Down => 2,
            Orientation::Left => 3,
        }
    }
}

impl From<i32> for Orientation {
    fn from(value: i32) -> Self {
        match value {
            -1 => Orientation::Left,
            0 => Orientation::Up,
            1 => Orientation::Right,
            2 => Orientation::Down,
            3 => Orientation::Left,
            _ => Orientation::Up,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Position {
    pub pos_x: i32,
    pub pos_y: i32,
}

impl Position {
    pub fn new(pos_x: i32, pos_y: i32) -> Self {
        Self { pos_x, pos_y }
    }

    pub fn default() -> Self {
        Self::new(0, 0)
    }

    pub fn forward(self, orientation: &Orientation) -> Self {
        match orientation {
            Orientation::Up => Self::new(self.pos_x, self.pos_y - 1),
            Orientation::Right => Self::new(self.pos_x + 1, self.pos_y),
            Orientation::Down => Self::new(self.pos_x, self.pos_y + 1),
            Orientation::Left => Self::new(self.pos_x - 1, self.pos_y),
        }
    }
}
