#[cfg(test)]
mod tests;

pub mod amplifier;
pub mod error;
pub mod instruction;
pub mod memory;
pub mod opcode;
pub mod orientation;
pub mod phasegenerator;
pub mod process;
pub mod robot;

pub use error::Error;
pub use memory::Memory;
pub use orientation::{Orientation, Position, Turn};
pub use phasegenerator::PhaseGenerator;
pub use process::Process;
pub use robot::HullPaintingRobot;
