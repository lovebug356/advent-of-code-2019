pub struct PhaseGenerator {
    size: usize,
    min: i64,
    max: i64,
}

impl PhaseGenerator {
    pub fn new(size: usize, min: i64, max: i64) -> Self {
        Self { size, min, max }
    }

    pub fn iter(&self) -> impl Iterator<Item = Vec<i64>> {
        let min = self.min;
        let max = self.max;
        let mut result: Box<dyn Iterator<Item = Vec<i64>>> =
            Box::new(single_phase_generator(min, max, vec![]));

        for _idx in 1..self.size {
            result =
                Box::new(result.flat_map(move |input| single_phase_generator(min, max, input)));
        }

        result
    }
}

fn single_phase_generator(min: i64, max: i64, input: Vec<i64>) -> impl Iterator<Item = Vec<i64>> {
    (min..max + 1).filter_map(move |idx| {
        if !input.iter().any(|&peer| peer == idx) {
            let mut result = input.clone();
            result.push(idx);
            Some(result)
        } else {
            None
        }
    })
}
