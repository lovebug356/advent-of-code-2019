use std::collections::HashMap;

use crate::{Error, Memory, Orientation, Position, Process};

pub struct HullPaintingRobot {
    memory: Memory,
    hull: HashMap<Position, i64>,
}

impl HullPaintingRobot {
    pub fn with_hull(mut self, hull: HashMap<Position, i64>) -> Self {
        self.hull = hull;
        self
    }

    pub fn paint_white(mut self) -> Self {
        self.hull.insert(Position::new(0, 0), 1);
        self
    }

    pub fn run(self) -> Result<Self, Error> {
        let mut position = Position::default();
        let mut orientation = Orientation::Up;
        let mut hull = self.hull;
        let mut process = Process::new(self.memory, 0, vec![]);
        loop {
            let current_color = hull.get(&position).copied().unwrap_or(0);
            process = process.with_input(vec![current_color]);
            process = match process.run() {
                Ok(process) => return Ok(Self::from(process.memory).with_hull(hull)),
                Err(err) => match err {
                    Error::NeedInput(proc) => proc,
                    _ => return Err(err),
                },
            };
            let result = process.reset_output();
            process = result.0;
            let output = result.1;
            hull.insert(position.clone(), output[0]);
            orientation = orientation.rotate(output[1].into());
            position = position.forward(&orientation);
        }
    }

    pub fn painted_panels(&self) -> usize {
        self.hull.len()
    }

    pub fn dump_message(&self) {
        let min_x = self.hull.keys().map(|x| x.pos_x).min().unwrap_or(0);
        let max_x = self.hull.keys().map(|x| x.pos_x).max().unwrap_or(0);
        let min_y = self.hull.keys().map(|x| x.pos_y).min().unwrap_or(0);
        let max_y = self.hull.keys().map(|x| x.pos_y).max().unwrap_or(0);
        for y in min_y..max_y + 1 {
            (min_x..max_x + 1).for_each(|x| {
                let color = self.hull.get(&Position::new(x, y)).unwrap_or(&0);
                match color {
                    1 => print!("#"),
                    _ => print!(" "),
                }
            });
            println!("");
        }
    }
}

impl From<Memory> for HullPaintingRobot {
    fn from(memory: Memory) -> Self {
        Self {
            memory,
            hull: HashMap::new(),
        }
    }
}
