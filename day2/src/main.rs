use std::convert::TryFrom;

const INPUT: &str = "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,6,19,23,2,23,6,27,1,5,27,31,1,31,9,35,2,10,35,39,1,5,39,43,2,43,10,47,1,47,6,51,2,51,6,55,2,55,13,59,2,6,59,63,1,63,5,67,1,6,67,71,2,71,9,75,1,6,75,79,2,13,79,83,1,9,83,87,1,87,13,91,2,91,10,95,1,6,95,99,1,99,13,103,1,13,103,107,2,107,10,111,1,9,111,115,1,115,10,119,1,5,119,123,1,6,123,127,1,10,127,131,1,2,131,135,1,135,10,0,99,2,14,0,0";

#[derive(Debug)]
enum Error {
    ParseError,
    OutOffBounds,
    ProgramFailed,
    NotFound,
}

enum Memory {
    OrigData(Vec<u32>),
    WriteData {
        data: Box<Memory>,
        address: usize,
        new_value: u32,
    },
}

impl Memory {
    pub fn len(&self) -> usize {
        match &*self {
            Memory::OrigData(data) => data.len(),
            Memory::WriteData { data, .. } => data.len(),
        }
    }

    pub fn read(&self, address: usize) -> Result<u32, Error> {
        match &*self {
            Memory::OrigData(data) => {
                if address > data.len() {
                    return Err(Error::OutOffBounds);
                }
                Ok(data[address as usize])
            }
            Memory::WriteData {
                data,
                address: write_address,
                new_value,
            } => {
                if address == *write_address {
                    Ok(*new_value)
                } else {
                    data.read(address)
                }
            }
        }
    }

    pub fn write(self, address: usize, new_value: u32) -> Result<Memory, Error> {
        if address > self.len() {
            return Err(Error::OutOffBounds);
        }
        Ok(Memory::WriteData {
            data: Box::new(self),
            address,
            new_value,
        })
    }
}

impl TryFrom<&str> for Memory {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Ok(Memory::OrigData(
            value
                .split(",")
                .map(|s| s.parse::<u32>().map_err(|_e| Error::ParseError))
                .collect::<Result<_, _>>()?,
        ))
    }
}

struct Program {
    position: usize,
    memory: Memory,
}

struct Computer {}

impl Computer {
    pub fn new() -> Computer {
        Computer {}
    }
    pub fn run(&self, memory: Memory) -> Result<Memory, Error> {
        let mut program = Program {
            position: 0,
            memory,
        };
        loop {
            program = Program {
                position: program.position + 4,
                memory: match program.memory.read(program.position)? {
                    1 => self.add(program)?,
                    2 => self.mul(program)?,
                    99 => return Ok(program.memory),
                    _ => return Err(Error::ProgramFailed),
                },
            }
        }
    }

    fn add(&self, program: Program) -> Result<Memory, Error> {
        let memory = program.memory;
        let pointer = program.position;
        let param_1 = memory.read(pointer + 1)? as usize;
        let param_2 = memory.read(pointer + 2)? as usize;
        let dest = memory.read(pointer + 3)? as usize;
        let result = memory.read(param_1)? + memory.read(param_2)?;
        Ok(memory.write(dest, result)?)
    }

    fn mul(&self, program: Program) -> Result<Memory, Error> {
        let memory = program.memory;
        let pointer = program.position;
        let param_1 = memory.read(pointer + 1)? as usize;
        let param_2 = memory.read(pointer + 2)? as usize;
        let dest = memory.read(pointer + 3)? as usize;
        let result = memory.read(param_1)? * memory.read(param_2)?;
        Ok(memory.write(dest, result)?)
    }
}

fn main() -> Result<(), Error> {
    let orig_memory = Memory::try_from(INPUT)?;
    let memory = orig_memory.write(1, 12)?;
    let memory = memory.write(2, 2)?;
    let computer = Computer::new();
    let memory = computer.run(memory)?;
    println!("Phase 1");
    println!("Memory at pointer 0: {}", memory.read(0)?);

    for noun in 0..99 {
        for verb in 0..99 {
            let orig_memory = Memory::try_from(INPUT)?;
            let noun_memory = orig_memory.write(1, noun)?;
            let memory = noun_memory.write(2, verb)?;
            let computer = Computer::new();
            let memory = computer.run(memory)?;
            if memory.read(0)? == 19690720 {
                println!("Phase 2");
                println!("100 * noun * verb = {}", 100 * noun + verb);
                return Ok(());
            }
        }
    }

    Err(Error::NotFound)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_memory() -> Result<(), Error> {
        let memory = Memory::try_from("0,1,2,3")?;
        assert_eq!(memory.read(0)?, 0);
        assert_eq!(memory.read(1)?, 1);
        assert_eq!(memory.read(2)?, 2);
        assert_eq!(memory.read(3)?, 3);

        Ok(())
    }

    #[test]
    fn write_memory() -> Result<(), Error> {
        let memory = Memory::try_from("0,1,2")?;
        let memory = memory.write(1, 3)?;
        assert_eq!(memory.read(1)?, 3);

        let memory = memory.write(0, 4)?;
        assert_eq!(memory.read(0)?, 4);

        let memory = memory.write(2, 5)?;
        assert_eq!(memory.read(2)?, 5);

        Ok(())
    }

    #[test]
    fn run_program() -> Result<(), Error> {
        let memory = Memory::try_from("1,0,0,0,99")?;
        let computer = Computer::new();
        let memory = computer.run(memory)?;
        assert_eq!(memory.read(0)?, 2);

        let memory = Memory::try_from("2,3,0,3,99")?;
        let computer = Computer::new();
        let memory = computer.run(memory)?;
        assert_eq!(memory.len(), 5);
        assert_eq!(memory.read(3)?, 6);

        let memory = Memory::try_from("2,4,4,5,99,0")?;
        let computer = Computer::new();
        let memory = computer.run(memory)?;
        assert_eq!(memory.read(5)?, 9801);

        let memory = Memory::try_from("1,1,1,4,99,5,6,0,99")?;
        let computer = Computer::new();
        let memory = computer.run(memory)?;
        assert_eq!(memory.read(0)?, 30);
        assert_eq!(memory.read(4)?, 2);

        Ok(())
    }
}
