use std::convert::TryFrom;

use crate::memory::Memory;
use crate::process::Process;
use crate::Error;

#[test]
fn from_memory() {
    let memory = Memory::try_from("1,2,3").unwrap();
    let process = Process::from(memory);

    assert_eq!(process.pointer, 0);
}

#[test]
fn process_run() -> Result<(), Error> {
    let memory = Memory::try_from("1101,100,-1,4,0")?;
    let process = Process::from(memory);
    let memory = process.run()?;
    assert_eq!(memory.read(4)?, 99);

    let memory = Memory::try_from("101,100,1,5,99,0")?;
    let process = Process::from(memory);
    let memory = process.run()?;
    assert_eq!(memory.read(5)?, 200);

    let memory = Memory::try_from("1102,2,3,5,99,0")?;
    let process = Process::from(memory);
    let memory = process.run()?;
    assert_eq!(memory.read(5)?, 6);

    let memory = Memory::try_from("1,0,0,0,99")?;
    let computer = Process::from(memory);
    let memory = computer.run()?;
    assert_eq!(memory.read(0)?, 2);

    let memory = Memory::try_from("2,3,0,3,99")?;
    let computer = Process::from(memory);
    let memory = computer.run()?;
    assert_eq!(memory.len(), 5);
    assert_eq!(memory.read(3)?, 6);

    let memory = Memory::try_from("2,4,4,5,99,0")?;
    let computer = Process::from(memory);
    let memory = computer.run()?;
    assert_eq!(memory.read(5)?, 9801);

    let memory = Memory::try_from("1,1,1,4,99,5,6,0,99")?;
    let computer = Process::from(memory);
    let memory = computer.run()?;
    assert_eq!(memory.read(0)?, 30);
    assert_eq!(memory.read(4)?, 2);

    let memory = Memory::try_from("3,9,8,9,10,9,4,9,99,-1,8")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(0);
    let memory = computer.run()?;
    assert_eq!(memory.read(9)?, 0);

    let memory = Memory::try_from("3,9,8,9,10,9,4,9,99,-1,8")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(8);
    let memory = computer.run()?;
    assert_eq!(memory.read(9)?, 1);

    let memory = Memory::try_from("3,9,7,9,10,9,4,9,99,-1,8")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(8);
    let memory = computer.run()?;
    assert_eq!(memory.read(9)?, 0);

    let memory = Memory::try_from("3,9,7,9,10,9,4,9,99,-1,8")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(7);
    let memory = computer.run()?;
    assert_eq!(memory.read(9)?, 1);

    let memory = Memory::try_from("3,3,1108,-1,8,3,4,3,99")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(8);
    let memory = computer.run()?;
    assert_eq!(memory.read(3)?, 1);

    let memory = Memory::try_from("3,3,1108,-1,8,3,4,3,99")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(7);
    let memory = computer.run()?;
    assert_eq!(memory.read(3)?, 0);

    let memory = Memory::try_from("3,3,1107,-1,8,3,4,3,99")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(7);
    let memory = computer.run()?;
    assert_eq!(memory.read(3)?, 1);

    let memory = Memory::try_from("3,3,1107,-1,8,3,4,3,99")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(8);
    let memory = computer.run()?;
    assert_eq!(memory.read(3)?, 0);

    let memory = Memory::try_from("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(0);
    let memory = computer.run()?;
    assert_eq!(memory.read(13)?, 0);

    let memory = Memory::try_from("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9")?;
    let computer = Process::from(memory);
    let computer = computer.with_input(1);
    let memory = computer.run()?;
    assert_eq!(memory.read(13)?, 1);

    Ok(())
}
