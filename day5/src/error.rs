#[derive(Debug)]
pub enum Error {
    ParseError,
    OutOffBounds(String),
    ProgramFailed,
    NotFound,
    InvalidOpcode(String),
}
