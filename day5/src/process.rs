use std::convert::TryFrom;

use crate::instruction::Instruction;
use crate::instruction::ParameterMode;
use crate::memory::Memory;
use crate::opcode::Opcode;
use crate::Error;

pub struct Process {
    pub memory: Memory,
    pub pointer: usize,
    pub input: i32,
}

impl Process {
    pub fn with_input(self, input: i32) -> Process {
        Process {
            memory: self.memory,
            pointer: self.pointer,
            input,
        }
    }

    pub fn run(self) -> Result<Memory, Error> {
        let mut program = self;
        loop {
            let opcode = program.memory.read(program.pointer)?;
            let instruction = Instruction::try_from(opcode)?;
            println!("opcode: {:?}", instruction);
            program = match instruction.opcode {
                Opcode::Add => program.add()?,
                Opcode::Multiply => program.mul()?,
                Opcode::Save => program.save()?,
                Opcode::Output => program.output()?,
                Opcode::JumpIfTrue => program.jump_true()?,
                Opcode::JumpIfFalse => program.jump_false()?,
                Opcode::LessThan => program.less_than()?,
                Opcode::Equals => program.equals()?,
                Opcode::EndOfProgram => return Ok(program.memory),
            }
        }
    }

    fn add(self) -> Result<Process, Error> {
        let pointer = self.pointer;
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let memory = self.memory;
        let dest = memory.read(pointer + 3)? as usize;
        let result = param_1 + param_2;
        println!("{} + {} = {} (to {})", param_1, param_2, result, dest);
        Ok(Process {
            pointer: pointer + 4,
            memory: memory.write(dest, result)?,
            input: self.input,
        })
    }

    fn mul(self) -> Result<Process, Error> {
        let pointer = self.pointer;
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let memory = self.memory;
        let dest = memory.read(pointer + 3)? as usize;
        let result = param_1 * param_2;
        Ok(Process {
            pointer: pointer + 4,
            memory: memory.write(dest, result)?,
            input: self.input,
        })
    }

    fn save(self) -> Result<Process, Error> {
        // let param_1 = self.read_param(0)?;
        let param_1 = self.memory.read(self.pointer + 1)?;
        Ok(Process {
            pointer: self.pointer + 2,
            memory: self.memory.write(param_1 as usize, self.input)?,
            input: self.input,
        })
    }

    fn output(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        println!("Output: {}", param_1);
        Ok(Process {
            pointer: self.pointer + 2,
            memory: self.memory,
            input: self.input,
        })
    }

    fn jump_true(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        Ok(Process {
            pointer: if param_1 == 0 {
                self.pointer + Opcode::JumpIfTrue.size()
            } else {
                self.read_param(1)? as usize
            },
            memory: self.memory,
            input: self.input,
        })
    }

    fn jump_false(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        Ok(Process {
            pointer: if param_1 == 0 {
                self.read_param(1)? as usize
            } else {
                self.pointer + Opcode::JumpIfFalse.size()
            },
            memory: self.memory,
            input: self.input,
        })
    }

    fn less_than(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let param_3 = self.memory.read(self.pointer + 3)? as usize;
        Ok(Process {
            pointer: self.pointer + Opcode::LessThan.size(),
            memory: self
                .memory
                .write(param_3, if param_1 < param_2 { 1 } else { 0 })?,
            input: self.input,
        })
    }

    fn equals(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let param_3 = self.memory.read(self.pointer + 3)? as usize;
        Ok(Process {
            pointer: self.pointer + Opcode::Equals.size(),
            memory: self
                .memory
                .write(param_3, if param_1 == param_2 { 1 } else { 0 })?,
            input: self.input,
        })
    }

    fn read_param(&self, idx: usize) -> Result<i32, Error> {
        let opcode = self.memory.read(self.pointer)?;
        let instruction = Instruction::try_from(opcode)?;
        let mode = &instruction.parameter_mode[idx];
        let position = self.memory.read(self.pointer + idx + 1)?;
        Ok(match mode {
            ParameterMode::PositionMode => self.memory.read(position as usize)?,
            ParameterMode::ImmediateMode => position,
        })
    }
}

impl From<Memory> for Process {
    fn from(memory: Memory) -> Self {
        Self {
            pointer: 0,
            memory,
            input: 0,
        }
    }
}
