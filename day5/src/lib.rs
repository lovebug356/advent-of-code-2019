#[cfg(test)]
mod tests;

pub mod error;
pub use error::Error;

pub mod instruction;
pub mod memory;
pub mod opcode;
pub mod process;
