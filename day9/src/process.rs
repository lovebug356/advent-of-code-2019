use std::convert::TryFrom;

use crate::instruction::Instruction;
use crate::instruction::ParameterMode;
use crate::memory::Memory;
use crate::opcode::Opcode;
use crate::Error;

#[derive(Debug, Clone)]
pub struct Process {
    pub memory: Memory,
    pub pointer: usize,
    pub input: Vec<i64>,
    pub relative_base: i64,
    pub output: Vec<i64>,
}

impl Process {
    pub fn new(memory: Memory, pointer: usize, input: Vec<i64>) -> Self {
        Self {
            memory,
            pointer,
            input,
            relative_base: 0,
            output: vec![],
        }
    }

    pub fn with_input(mut self, input: Vec<i64>) -> Self {
        self.input = input;
        self
    }

    pub fn append_input(mut self, input: Vec<i64>) -> Self {
        input.iter().for_each(|&e| self.input.push(e));
        self
    }

    pub fn reset_output(mut self) -> (Self, Vec<i64>) {
        let output = self.output;
        self.output = vec![];
        (self, output)
    }

    pub fn with_relative_base(mut self, base: i64) -> Self {
        self.relative_base = base;
        self
    }

    pub fn with_single_input(mut self, input: i64) -> Self {
        self.input = vec![input];
        self
    }

    pub fn pop_front_input(mut self) -> Result<(Self, i64), Error> {
        if self.input.len() == 0 {
            return Err(Error::NeedInput(self));
        }
        let next_input = self.input.remove(0);
        Ok((self, next_input))
    }

    pub fn with_memory(mut self, memory: Memory) -> Self {
        self.memory = memory;
        self
    }

    pub fn with_increased_pointer(mut self, offset: usize) -> Self {
        self.pointer += offset;
        self
    }

    pub fn write_memory(mut self, position: usize, value: i64) -> Result<Self, Error> {
        self.memory = self.memory.write(position, value)?;
        Ok(self)
    }

    pub fn run(self) -> Result<Process, Error> {
        let mut program = self;
        loop {
            let opcode = program.memory.read(program.pointer)?;
            let instruction = Instruction::try_from(opcode)?;
            program = match instruction.opcode {
                Opcode::Add => program.add()?,
                Opcode::Multiply => program.mul()?,
                Opcode::Save => program.save()?,
                Opcode::Output => program.output()?,
                Opcode::JumpIfTrue => program.jump_true()?,
                Opcode::JumpIfFalse => program.jump_false()?,
                Opcode::LessThan => program.less_than()?,
                Opcode::Equals => program.equals()?,
                Opcode::AdjustRelativeBase => program.adjust_relative_base()?,
                Opcode::EndOfProgram => return Ok(program),
            }
        }
    }

    fn add(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let dest = self.write_param(2)? as usize;
        let result = param_1 + param_2;
        Ok(self
            .write_memory(dest, result)?
            .with_increased_pointer(Opcode::Add.size()))
    }

    fn mul(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let dest = self.write_param(2)? as usize;
        let result = param_1 * param_2;
        Ok(self
            .write_memory(dest, result)?
            .with_increased_pointer(Opcode::Multiply.size()))
    }

    fn save(self) -> Result<Process, Error> {
        let param_1 = self.write_param(0)?;
        let (result, next_input) = self.pop_front_input()?;
        println!("save {} to memory {}", next_input, param_1);
        Ok(result
            .write_memory(param_1 as usize, next_input)?
            .with_increased_pointer(Opcode::Save.size()))
    }

    fn output(mut self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        println!("Output: {}", param_1);
        self.output.push(param_1);
        Ok(self.with_increased_pointer(Opcode::Output.size()))
    }

    fn jump_true(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        Ok(Process {
            pointer: if param_1 == 0 {
                self.pointer + Opcode::JumpIfTrue.size()
            } else {
                self.read_param(1)? as usize
            },
            memory: self.memory,
            input: self.input,
            relative_base: self.relative_base,
            output: self.output,
        })
    }

    fn jump_false(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        Ok(Process {
            pointer: if param_1 == 0 {
                self.read_param(1)? as usize
            } else {
                self.pointer + Opcode::JumpIfFalse.size()
            },
            memory: self.memory,
            input: self.input,
            relative_base: self.relative_base,
            output: self.output,
        })
    }

    fn less_than(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let param_3 = self.write_param(2)? as usize;
        Ok(Process {
            pointer: self.pointer + Opcode::LessThan.size(),
            memory: self
                .memory
                .write(param_3, if param_1 < param_2 { 1 } else { 0 })?,
            input: self.input,
            relative_base: self.relative_base,
            output: self.output,
        })
    }

    fn equals(self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        let param_2 = self.read_param(1)?;
        let param_3 = self.write_param(2)? as usize;
        Ok(Process {
            pointer: self.pointer + Opcode::Equals.size(),
            memory: self
                .memory
                .write(param_3, if param_1 == param_2 { 1 } else { 0 })?,
            input: self.input,
            relative_base: self.relative_base,
            output: self.output,
        })
    }

    fn adjust_relative_base(mut self) -> Result<Process, Error> {
        let param_1 = self.read_param(0)?;
        self.relative_base += param_1;
        Ok(self.with_increased_pointer(Opcode::AdjustRelativeBase.size()))
    }

    fn read_param(&self, idx: usize) -> Result<i64, Error> {
        let opcode = self.memory.read(self.pointer)?;
        let instruction = Instruction::try_from(opcode)?;
        let mode = &instruction.parameter_mode[idx];
        let position = self.memory.read(self.pointer + idx + 1)?;
        Ok(match mode {
            ParameterMode::PositionMode => self.memory.read(position as usize)?,
            ParameterMode::ImmediateMode => position,
            ParameterMode::RelativeMode => {
                self.memory.read((position + self.relative_base) as usize)?
            }
        })
    }

    fn write_param(&self, idx: usize) -> Result<i64, Error> {
        let opcode = self.memory.read(self.pointer)?;
        let instruction = Instruction::try_from(opcode)?;
        let mode = &instruction.parameter_mode[idx];
        let position = self.memory.read(self.pointer + idx + 1)?;
        Ok(match mode {
            ParameterMode::PositionMode => position,
            ParameterMode::ImmediateMode => position,
            ParameterMode::RelativeMode => (position + self.relative_base),
        })
    }
}

impl From<Memory> for Process {
    fn from(memory: Memory) -> Self {
        Self::new(memory, 0, vec![0])
    }
}
