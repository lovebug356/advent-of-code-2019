use std::convert::TryFrom;

use day9::memory::Memory;
use day9::process::Process;
use day9::Error;

fn main() -> Result<(), Error> {
    let contents = std::fs::read_to_string("input.txt").map_err(|_err| Error::FileError)?;
    let memory = Memory::try_from(contents.as_ref())?;

    println!("Run in test mode");
    let _process = Process::from(memory.clone()).with_single_input(1).run()?;

    println!("Run in sensor boost mode");
    let _process = Process::from(memory).with_single_input(2).run()?;
    Ok(())
}
