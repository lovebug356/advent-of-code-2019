use std::convert::TryFrom;

use crate::opcode::Opcode;
use crate::Error;

#[derive(Debug, Clone, PartialEq)]
pub enum ParameterMode {
    PositionMode,
    ImmediateMode,
    RelativeMode,
}

impl TryFrom<i64> for ParameterMode {
    type Error = Error;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(ParameterMode::PositionMode),
            1 => Ok(ParameterMode::ImmediateMode),
            2 => Ok(ParameterMode::RelativeMode),
            _ => Err(Error::UnknownParameterMode(format!(
                "unknown parameter mode {}",
                value
            ))),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Instruction {
    pub opcode: Opcode,
    pub parameter_mode: Vec<ParameterMode>,
}

impl TryFrom<i64> for Instruction {
    type Error = Error;

    fn try_from(value: i64) -> Result<Self, Self::Error> {
        let opcode = Opcode::try_from(value % 100)?;
        let parameter_mode = vec![
            ParameterMode::try_from((value / 100) % 10)?,
            ParameterMode::try_from((value / 1000) % 10)?,
            ParameterMode::try_from((value / 10000) % 10)?,
        ];
        Ok(Instruction {
            opcode,
            parameter_mode,
        })
    }
}
