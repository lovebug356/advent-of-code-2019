use std::convert::TryFrom;

use crate::amplifier::Amplifier;
use crate::memory::Memory;
use crate::Error;

#[test]
fn example_1() -> Result<(), Error> {
    let memory = Memory::try_from("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0").unwrap();
    let amplifier = Amplifier::new(memory);
    let phases = vec![4, 3, 2, 1, 0];

    let output = amplifier.run_series(0, &phases)?;

    assert_eq!(output, 43210);
    assert_eq!(amplifier.search_max_output()?, 43210);

    Ok(())
}

#[test]
fn example_2() -> Result<(), Error> {
    let memory = Memory::try_from(
        "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0",
    )
    .unwrap();
    let amplifier = Amplifier::new(memory);
    let phases = vec![0, 1, 2, 3, 4];

    let output = amplifier.run_series(0, &phases)?;

    assert_eq!(output, 54321);
    assert_eq!(amplifier.search_max_output()?, 54321);

    Ok(())
}

#[test]
fn example_3() -> Result<(), Error> {
    let memory = Memory::try_from(
        "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0",
    )
    .unwrap();
    let amplifier = Amplifier::new(memory);
    let phases = vec![1, 0, 4, 3, 2];

    let output = amplifier.run_series(0, &phases)?;

    assert_eq!(output, 65210);
    assert_eq!(amplifier.search_max_output()?, 65210);

    Ok(())
}

#[test]
fn example_1_loop() -> Result<(), Error> {
    let memory = Memory::try_from(
        "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5",
    )
    .unwrap();
    let amplifier = Amplifier::new(memory);

    let phases = vec![9, 8, 7, 6, 5];
    let output = amplifier.run_series_loop(0, &phases)?;

    assert_eq!(output, 139629729);
    assert_eq!(amplifier.search_max_output_loop()?, 139629729);

    Ok(())
}

#[test]
fn example_2_loop() -> Result<(), Error> {
    let memory = Memory::try_from(
        "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10",
    )
    .unwrap();
    let amplifier = Amplifier::new(memory);

    let phases = vec![9, 7, 8, 5, 6];
    let output = amplifier.run_series_loop(0, &phases)?;

    assert_eq!(output, 18216);
    assert_eq!(amplifier.search_max_output_loop()?, 18216);

    Ok(())
}
